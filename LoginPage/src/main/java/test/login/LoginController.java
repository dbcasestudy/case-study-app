package test.login;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginController
 */
public class LoginController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String un = request.getParameter("username").trim();
        String pw = request.getParameter("password").trim();
        
        //TODO: add check for not null/not empty

        // Connect to mysql and verify username password
        try {
            Class.forName("com.mysql.jdbc.Driver");
            // loads driver
            Connection c = DriverManager.getConnection("jdbc:mysql://localhost:3307/db_grad", "root", "ppp"); // gets a new connection

            PreparedStatement ps = c.prepareStatement("select user_id,user_pwd from users where user_id=? and user_pwd=?");
            ps.setString(1, un);
            ps.setString(2, pw);

            ResultSet rs = ps.executeQuery();

            response.setContentType("text/plain");

            if (rs.next()) {
		response.getWriter().write("success");
            } else {
                response.getWriter().write("unsuccess");
            }
        } catch (ClassNotFoundException | SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
